/*
    A tool for converting a truecolor hexadecimal representation of a color
    to an 8bit representation: rgb with range 0-5.

    Copyright (C)
        Ryan Jeffrey 2020

    Results:
        0: Success
        1: No input file
        2: Something failed to be converted
*/
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cmath>

// representation of an 8bit color
struct eightBColors
{
    short r = 0;
    short g = 0;
    short b = 0;
    std::string str; // string that the 8bit color was generated from

    friend std::ostream &operator<<(std::ostream &os, const eightBColors &cols)
    {
        os << cols.r << cols.g << cols.b;
        return os;
    }

    bool operator==(const eightBColors &other)
    {
        if(r != other.r || g != other.g || b != other.b)
            return false;
        return true;
    }

    bool operator!=(const eightBColors &other)
    {
        return !((*this) == other);
    }

    explicit operator bool()
    {
        if(r == -1 || g == -1 || b == -1)
            return false;
        return true;
    }
};

eightBColors error = {-1, -1, -1}; // error value

// convert integer (range 0 - 255) to short (range 0 - 5)
inline short intToRGBChar(int i)
{
    if(i < 0 || i > 255)
        return -1;
    double d = static_cast<double>(i);
    return static_cast<short>(round(5.0 / (255.0 / d)));
}

eightBColors strToColors(std::string &str)
{
    auto makeErrorStruct = [&]()
    {
        eightBColors e = error;
        e.str = str;
        return e;
    };

    if(str.size() != 6)
        return makeErrorStruct();
    auto strFromChars = [=](char c1, char c2)
    {
        std::string s = "";
        s += c1;
        s += c2;
        return s;
    };
    // convert hexadecimal string to integer
    std::stringstream r;
    r << strFromChars(str[0], str[1]);
    std::stringstream g;
    g << strFromChars(str[2], str[3]);
    std::stringstream b;
    b << strFromChars(str[4], str[5]);

    if(r.fail() || g.fail() || b.fail())
        return makeErrorStruct();

    int ir;
    int ig;
    int ib;

    r >> std::hex >> ir;
    g >> std::hex >> ig;
    b >> std::hex >> ib;

    return { intToRGBChar(ir), intToRGBChar(ig), intToRGBChar(ib), str };
}

std::vector<eightBColors> colorsOfDouble(const char *path)
{
    std::vector<eightBColors> result;
    std::ifstream jcf(path);
    std::string line;
    bool parse = false;

    if(!jcf.is_open())
    {
        std::cerr << "Could not open file " << path << '\n';
        return result;
    }

    while(std::getline(jcf >> std::ws, line) && !line.empty())
    {
        if(line[0] != '.')
            continue;

        if(!parse && line.find(".colors 256") != std::string::npos)
        {
            parse = true;
        }
        else if(parse && line.find(".colors *") != std::string::npos)
        {
            break;
        }
        else if(parse)
        {
            std::size_t dollarIndex = line.find('$', 6);
            if(dollarIndex != std::string::npos)
            {
                std::string rgb = line.substr(dollarIndex + 1);
                result.push_back(strToColors(rgb));
            }
        }
    }

    jcf.close();

    return result;
}

int main(const int argc, const char **argv)
{
    if(argc != 2)
        return 1;

    auto colors = colorsOfDouble(argv[1]);
    int result = 0;

    for(auto &c : colors)
    {
        if(c)
            std::cout << c.str << " is now: " << c << '\n';
        else
        {
            result = 2;
            std::cerr << "Error converting " << c.str << '\n';
        }
    }

    return result;
}